-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Sep 2020 pada 18.24
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `18_lksn2020_server_module`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `boards`
--

CREATE TABLE `boards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `creator_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `boards`
--

INSERT INTO `boards` (`id`, `creator_id`, `name`, `created_at`, `updated_at`) VALUES
(7, 7, 'new board', '2020-09-09 01:33:24', '2020-09-09 01:33:24'),
(8, 7, 'new board 2', '2020-09-09 01:33:42', '2020-09-09 01:33:42'),
(9, 1, 'new board john', '2020-09-09 01:37:03', '2020-09-09 22:28:39'),
(11, 1, 'New again', '2020-09-09 22:28:46', '2020-09-09 22:28:46'),
(12, 1, 'New again 2', '2020-09-09 22:29:13', '2020-09-09 22:29:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `board_lists`
--

CREATE TABLE `board_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `board_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `board_lists`
--

INSERT INTO `board_lists` (`id`, `board_id`, `order`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 1, 'new list', '2020-09-09 06:13:55', '2020-09-09 06:32:47', NULL),
(3, 7, 2, 'new list 2', '2020-09-09 06:29:11', '2020-09-09 06:32:47', NULL),
(4, 8, 1, 'new list 8', '2020-09-09 07:15:55', '2020-09-09 07:15:55', NULL),
(5, 9, 1, 'New List John', '2020-09-09 23:49:26', '2020-09-10 00:19:56', NULL),
(7, 9, 2, 'List 2', '2020-09-10 00:00:31', '2020-09-10 01:59:58', '2020-09-10 01:59:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `board_members`
--

CREATE TABLE `board_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `board_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `board_members`
--

INSERT INTO `board_members` (`id`, `board_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 7, 7, NULL, NULL),
(2, 8, 7, NULL, NULL),
(3, 9, 1, NULL, NULL),
(7, 11, 1, NULL, NULL),
(8, 12, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cards`
--

CREATE TABLE `cards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `list_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `task` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cards`
--

INSERT INTO `cards` (`id`, `list_id`, `order`, `task`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'board task', '2020-09-09 07:07:31', '2020-09-09 07:09:45'),
(2, 1, 2, 'update task', '2020-09-09 07:07:44', '2020-09-09 07:19:13'),
(3, 5, 1, 'new card', '2020-09-10 00:00:22', '2020-09-10 00:00:22'),
(4, 7, 1, 'list 2 card', '2020-09-10 00:02:08', '2020-09-10 00:20:02'),
(5, 7, 2, 'list 2 card 22', '2020-09-10 00:11:22', '2020-09-10 00:37:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_09_063502_create_boards_table', 1),
(5, '2020_09_09_063542_create_board_lists_table', 1),
(6, '2020_09_09_063605_create_cards_table', 1),
(7, '2020_09_09_063651_create_board_members', 1),
(8, '2020_09_10_085533_add_field_deleted_at', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `token`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 'john.doe', '$2y$10$spYg71Zd1RjYPbhWDD3SYeRA.jVSTpxPIJ2kfgfGZQ5gEBdx6Iu8e', NULL, '2020-09-09 00:17:59', '2020-09-10 19:06:21'),
(2, 'Richard', 'Roe', 'richard.roe', '$2y$10$halzNMKkxmeoGfUfNKmAhuqEpEcqiScfHPVqQjCflMmaHhnhXjw0W', NULL, '2020-09-09 00:36:14', '2020-09-09 00:36:14'),
(7, 'competitor', 'indonesia', 'competitor', '$2y$10$QD.807K38j7TDSs3BAQWJ.cTYW2ImOKgIoQ5k5R/ViMxX7PGN2XV.', NULL, '2020-09-09 00:49:46', '2020-09-09 01:13:11'),
(11, 'Jane', 'Poe', 'jane.poe', '$2y$10$zxvmj0nSeoynk7ZMikdXjeSLAdvvTvla2hZA1f4S8raOvk4C31KO.', NULL, '2020-09-10 19:11:28', '2020-09-10 19:11:28');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boards_creator_id_foreign` (`creator_id`);

--
-- Indeks untuk tabel `board_lists`
--
ALTER TABLE `board_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_lists_board_id_foreign` (`board_id`);

--
-- Indeks untuk tabel `board_members`
--
ALTER TABLE `board_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_members_board_id_foreign` (`board_id`),
  ADD KEY `board_members_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cards_list_id_foreign` (`list_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `boards`
--
ALTER TABLE `boards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `board_lists`
--
ALTER TABLE `board_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `board_members`
--
ALTER TABLE `board_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `cards`
--
ALTER TABLE `cards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `boards`
--
ALTER TABLE `boards`
  ADD CONSTRAINT `boards_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `board_lists`
--
ALTER TABLE `board_lists`
  ADD CONSTRAINT `board_lists_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `board_members`
--
ALTER TABLE `board_members`
  ADD CONSTRAINT `board_members_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `board_members_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_list_id_foreign` FOREIGN KEY (`list_id`) REFERENCES `board_lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
