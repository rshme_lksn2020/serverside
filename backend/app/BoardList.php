<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoardList extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

    /* relations */

    public function cards(){
        return $this->hasMany(Card::class, 'list_id');
    }

    public function board(){
        return $this->belongsTo(Board::class, 'board_id');
    }
}
