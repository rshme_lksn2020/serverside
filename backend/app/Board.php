<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function creator(){
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function lists(){
        return $this->hasMany(BoardList::class, 'board_id');
    }

    public function members(){
        return $this->belongsToMany(User::class, 'board_members', 'board_id', 'user_id');
    }
}
