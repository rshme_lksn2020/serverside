<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $guarded = ['id'];

    public function list(){
        return $this->belongsTo(BoardList::class, 'list_id');
    }
}
