<?php

namespace App\Http\Middleware;

use App\Libs\Response;
use App\User;
use Closure;

class APITokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = request('token');

        if(!$token){
            $token = '';
        }

        $user = User::whereToken($token)->first();

        if($user){
            return $next($request);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }
}
