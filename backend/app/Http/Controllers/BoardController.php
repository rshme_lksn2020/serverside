<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Board;
use App\Libs\Response;
use Illuminate\Support\Facades\Validator;

class BoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('apiToken');
        $this->token = request('token');
        $this->user = \App\User::whereToken($this->token)->first();
    }

    public function index(){
        $boards = Board::whereHas('members', function($user){
            $user->whereUserId($this->user->id);
        })->get();

        return Response::success($boards);
    }

    public function show($board_id){
        $board = Board::with(['members', 'lists' => function($list){
            $list->with(['cards' => function($card){
                $card->orderBy('order', 'asc');
            }])->orderBy('order', 'asc');
        }])->whereHas('members', function($user){
            $user->whereUserId($this->user->id);
        })->whereId($board_id)->first();

        if($board){
            return Response::success($board);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function showDeleted($board_id){
        $board = Board::with(['members', 'lists' => function($list){
            $list->with(['cards' => function($card){
                $card->orderBy('order', 'asc');
            }])->withTrashed()->orderBy('order', 'asc');
        }])->whereHas('members', function($user){
            $user->whereUserId($this->user->id);
        })->whereId($board_id)->first();

        if($board){
            return Response::success($board);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function store(Request $request){
        /* validation */
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $board = Board::create([
            'name' => $request->name,
            'creator_id'=> $this->user->id
        ]);

        $board->members()->attach($this->user->id);

        return Response::success(['message' => 'create board success']);
    }

    public function update($board_id, Request $request){
        /* validation */
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $board = Board::findOrFail($board_id);
        $board->update([
            'name' => $request->name
        ]);

        return Response::success(['message' => 'update board success']);
    }

    public function destroy($board_id){
        $board = Board::findOrFail($board_id);

        if($board->creator_id === $this->user->id){
            $board->forceDelete();
            $board->delete();

            return Response::success(['message' => 'delete board success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function addMember($board_id, Request $request){
        $validation = Validator::make($request->all(), [
            'username' => 'exists:users,username'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'user did not exist']);
        }

        $user = \App\User::whereUsername($request->username)->first();

        $board = Board::whereHas('members', function($user){
            $user->whereUserId($this->user->id);
        })->whereId($board_id)->first();

        if($board){
            $board->members()->attach($user->id);

            return Response::success(['message' => 'add member success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function removeMember($board_id, $member){
        $board = Board::whereHas('members', function($user){
            $user->whereUserId($this->user->id);
        })->whereId($board_id)->first();

        if($board){
            $board->members()->detach($member);
            return Response::success(['message' => 'remove member success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }
}
