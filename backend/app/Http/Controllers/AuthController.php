<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\Response;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request){
        $data = $request->all();

        /* validation */
        $validation = Validator::make($data, [
            'first_name' => 'min:2|max:20|regex:/^[a-zA-Z]+$/',
            'last_name' => 'min:2|max:20|regex:/^[a-zA-Z]+$/',
            'username' => 'min:5|max:12|regex:/^[a-zA-Z0-9_.]+$/|unique:users,username',
            'password' => 'min:5|max:12'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $data['password'] = bcrypt($request->password);

        $user = User::create($data);
        $user->update([
            'token' => bcrypt($user->id)
        ]);

        Auth::loginUsingId($user->id);

        /* send response */
        $response = [
            'user' => $user->first_name . ' ' . $user->last_name,
            'token' => $user->token,
            'role' => 'user'
        ];

        return Response::success($response);
    }

    public function login(Request $request){
        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        if(Auth::attempt($credentials)){
            $user = Auth::user();

            $user->update([
                'token' => bcrypt($user->id)
            ]);

            $response = [
                'user' => $user->first_name . ' ' . $user->last_name,
                'token' => $user->token,
                'role' => 'user'
            ];

            return Response::success($response);
        }

        return Response::invalid(['message' => 'invalid login']);
    }

    public function logout(Request $request){
        $user = User::whereToken($request->token)->first();

        if($user){
            $user->update([
                'token' => NULL
            ]);

            return Response::success(['message' => 'logout success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }
}
