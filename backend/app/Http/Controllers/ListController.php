<?php

namespace App\Http\Controllers;

use App\Board;
use Illuminate\Http\Request;
use App\Libs\Response;
use App\BoardList;
use Illuminate\Support\Facades\Validator;

class ListController extends Controller
{
    public function __construct()
    {
        $this->middleware('apiToken');
        $this->token = request('token');
        $this->user = \App\User::whereToken($this->token)->first();
    }

    public function store($board_id, Request $request){
        /* validation */
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $board = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($board){
            $current = BoardList::whereBoardId($board_id)->orderBy('order', 'desc')->first();

            if($current){
                $order = $current->order + 1;
            }
            else{
                $order = 1;
            }

            BoardList::create([
                'name' => $request->name,
                'board_id' => $board_id,
                'order' => $order
            ]);

            return Response::success(['message' => 'create list success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function update($board_id, $list_id, Request $request){
        /* validation */
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $board = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($board){
            $list = BoardList::findOrFail($list_id);
            $list->update([
                'name' => $request->name,
            ]);

            return Response::success(['message' => 'update list success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function destroy($board_id, $list_id){
        $isMember = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($isMember){
            $list = BoardList::findOrFail($list_id);
            $list->delete();

            return Response::success(['message' => 'delete list success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function moveRight($board_id, $list_id, Request $request){
        $board = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($board){
            $list = BoardList::findOrFail($list_id);
            $prevList = BoardList::whereBoardId($list->board_id)->where('order', '<', $list->order)->first();

            if($prevList){
                $list->update([
                    'order' => $prevList->order
                ]);

                $prevList->update([
                    'order' => $list->order + 1
                ]);

                return Response::success(['message' => 'move success']);
            }
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function moveLeft($board_id, $list_id, Request $request){
        $board = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($board){
            $list = BoardList::findOrFail($list_id);
            $nextList = BoardList::whereBoardId($list->board_id)->where('order', '>', $list->order)->first();

            if($nextList){
                $list->update([
                    'order' => $nextList->order
                ]);

                $nextList->update([
                    'order' => $list->order - 1
                ]);

                return Response::success(['message' => 'move success']);
            }
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }
}
