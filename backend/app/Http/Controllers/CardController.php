<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\Response;
use App\Card;
use App\BoardList;
use App\Board;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{
    public function __construct()
    {
        $this->middleware('apiToken');
        $this->token = request('token');
        $this->user = \App\User::whereToken($this->token)->first();
    }

    public function store($board_id,$list_id,  Request $request){
        /* validation */
        $validation = Validator::make($request->all(), [
            'task' => 'required'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $board = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($board){
            $current = Card::whereHas('list', function($list) use($board_id){
                $list->whereBoardId($board_id);
            })->whereListId($list_id)->first();

            if($current){
                $order = $current->order + 1;
            }
            else{
                $order = 1;
            }

            Card::create([
                'task' => $request->task,
                'list_id' => $list_id,
                'order' => $order
            ]);

            return Response::success(['message' => 'create card success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function update($board_id, $list_id, $card_id,  Request $request){
        /* validation */
        $validation = Validator::make($request->all(), [
            'task' => 'required'
        ]);

        if($validation->fails()){
            return Response::error(['message' => 'invalid field']);
        }

        $board = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($board){
            $card = Card::findOrFail($card_id);
            $card->update([
                'task' => $request->task,
            ]);

            return Response::success(['message' => 'update card success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function destroy($board_id, $list_id, $card_id){
        $isMember = Board::whereId($board_id)->whereHas('members', function($member){
            $member->whereUserId($this->user->id);
        })->first();

        if($isMember){
            $card = Card::findOrFail($card_id);
            $card->delete();

            return Response::success(['message' => 'delete card success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function moveUp($card_id, Request $request){

        $isMember = Card::whereHas('list', function($list){
            $list->whereHas('board', function($board){
                $board->whereHas('members', function($member){
                    $member->whereUserId($this->user->id);
                });
            });
        })->whereId($card_id)->first();

        if($isMember){
            $card = Card::findOrFail($card_id);
            $prevCard = Card::whereListId($card->list_id)->where('order', '<', $card->order)->first();

            if($prevCard){
                $card->update([
                    'order' => $prevCard->order
                ]);

                $prevCard->update([
                    'order' => $card->order + 1
                ]);

                return Response::success(['message' => 'move success']);
            }
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function moveDown($card_id, Request $request){

        $isMember = Card::whereHas('list', function($list){
            $list->whereHas('board', function($board){
                $board->whereHas('members', function($member){
                    $member->whereUserId($this->user->id);
                });
            });
        })->whereId($card_id)->first();

        if($isMember){
            $card = Card::findOrFail($card_id);
            $nextCard = Card::whereListId($card->list_id)->where('order', '>', $card->order)->first();

            if($nextCard){
                $card->update([
                    'order' => $nextCard->order
                ]);

                $nextCard->update([
                    'order' => $card->order - 1
                ]);

                return Response::success(['message' => 'move success']);
            }
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }

    public function moveList($card_id, $list_id){
        $card = Card::whereId($card_id)->first();
        $list = BoardList::whereId($list_id)->first();

        $isMember = Card::whereHas('list', function($list){
            $list->whereHas('board', function($board){
                $board->whereHas('members', function($member){
                    $member->whereUserId($this->user->id);
                });
            });
        })->whereId($card_id)->first();

        if($isMember){
            if($list->board_id !== $card->list->board_id){
                return Response::error(['message' => 'move list invalid']);
            }

            $card->update([
                'list_id' => $list_id
            ]);

            return Response::success(['message' => 'move success']);
        }

        return Response::invalid(['message' => 'unauthorized user']);
    }
}
