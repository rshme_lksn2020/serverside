<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $appends = ['initial'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','token', 'created_at', 'updated_at','pivot', 'username'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getInitialAttribute(){
        $first = str_split($this->attributes['first_name'])[0];
        $last = str_split($this->attributes['last_name'])[0];

        return strtoupper($first) . strtoupper($last);
    }

    /* relations */
    public function boards(){
        return $this->belongsToMany(Board::class, 'board_members', 'user_id', 'board_id');
    }
}
