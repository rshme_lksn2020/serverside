<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'v1'], function(){
    /* Auth routes */
    Route::group(['prefix' => '/auth'], function(){
        Route::post('/register', 'AuthController@register');
        Route::post('/login', 'AuthController@login');
        Route::get('/logout', 'AuthController@logout');
    });

    /* board routes */
    Route::get('/board', 'BoardController@index');
    Route::post('/board', 'BoardController@store');
    Route::group(['prefix' => '/board/{board_id}'], function(){
        Route::get('/', 'BoardController@show');
        Route::get('/all', 'BoardController@showDeleted');
        Route::put('/', 'BoardController@update');
        Route::delete('/', 'BoardController@destroy');

        /* members */
        Route::post('/member', 'BoardController@addMember');
        Route::delete('/member/{member}', 'BoardController@removeMember');

        /* list endpoints */
        Route::post('/list' ,'ListController@store');
        Route::group(['prefix' => '/list/{list_id}'], function(){
            Route::put('/' ,'ListController@update');
            Route::delete('/' ,'ListController@destroy');
            Route::post('/right' ,'ListController@moveRight');
            Route::post('/left' ,'ListController@moveLeft');

            /* card routes */
            Route::post('/card' ,'CardController@store');
            Route::put('/card/{card_id}' ,'CardController@update');
            Route::delete('/card/{card_id}' ,'CardController@destroy');
        });
    });

    Route::post('/card/{card_id}/up', 'CardController@moveUp');
    Route::post('/card/{card_id}/down', 'CardController@moveDown');
    Route::post('/card/{card_id}/move/{list_id}', 'CardController@moveList');
});
