<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
               'first_name' => 'John',
               'last_name' => 'Doe',
               'username' => 'john.doe',
               'password' => bcrypt('12345'),
            ],
            [
                'first_name' => 'Richard',
                'last_name' => 'Roe',
                'username' => 'richard.roe',
                'password' => bcrypt('12345'),
            ],
            [
                'first_name' => 'Jane',
                'last_name' => 'Poe',
                'username' => 'jane.poe',
                'password' => bcrypt('12345'),
            ]
        ];

        foreach($users as $user){
            \App\User::create($user);
        }
    }
}
